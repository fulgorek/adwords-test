<?php

$pdo = new PDO('mysql:host=db', 'root', 'root', array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_ERRMODE => PDO::ERRMODE_SILENT));
$pdo->exec('USE testDB');

include 'autoload.php';
use rOpenDev\DataTablesPHP\DataTable;

$columns = array(
    array(
        'data' => 'criteria_id',
        'title' => 'Criteria ID',
        'sFilter'    => array(
            'type' => 'text',
        ),
        'parent' => 'Parent',
    ),
    array(
        'data' => 'name',
        'title' => 'Name',
    ),
    array(
        'data' => 'canonical_name',
        'title' => 'Canonical Name',
    ),
    array(
        'data' => 'country_code',
        'title' => 'Country Code',
        'className' => 'right',
    ),
    array(
        'data' => 'target_type',
        'title' => 'Target Type',
        'className' => 'right',
    ),
    array(
        'data' => 'status',
        'title' => 'Status',
        'className' => 'right',
    )
);
$ajax = array(
    'uri' => $_SERVER["REQUEST_URI"],
    'type' => 'POST',
);
$dataTable = DataTable::instance('oTable');
$dataTable->setColumns($columns)
          ->setServerSide($ajax);
$dataTable->renderFilterOperators = false;
if (isset($_REQUEST['draw'])) {
    $dataTable->setFrom('adwords')->setPdoLink($pdo);
    $dataTable->exec($_REQUEST);
}
?>
<html>
  <head>
    <title>Adwords</title>
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link href="//cdn.datatables.net/1.10.3/css/jquery.dataTables.css" rel="stylesheet">
    <script src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.js"></script>
    <script>
    $(document).ready(function() {
      <?php echo $dataTable->getJavascript(); ?>
    });
    </script>
  </head>
  <body>

<?php echo $dataTable->getHtml();
