CREATE DATABASE /*!32312 IF NOT EXISTS*/`testDB` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `testDB`;

DROP TABLE IF EXISTS `adwords`;

/*Table structure for table `addons` */
CREATE TABLE `adwords` (
  `criteria_id` INT(12) DEFAULT NULL,
  `name` VARCHAR(80) NOT NULL,
  `canonical_name` VARCHAR(80) NOT NULL,
  `parent_id` INT(12) DEFAULT NULL,
  `country_code` VARCHAR(2) NOT NULL,
  `target_type` VARCHAR(80) NOT NULL,
  `status` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`criteria_id`)
);

LOAD DATA LOCAL INFILE '/tmp/adwords.csv'
INTO TABLE adwords
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
